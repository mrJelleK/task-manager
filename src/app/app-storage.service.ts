import {Injectable, OnDestroy} from '@angular/core';
import {Observable, ReplaySubject, Subscription} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppStorageService implements OnDestroy {
  tasks: Array<ITask> = [];
  _$: Array<Subscription> = [];
  private tasksIdInc = 0;
  private tasksReplaySubject: ReplaySubject<Array<ITask>> = new ReplaySubject(1);
  tasks$: Observable<Array<ITask>> = this.tasksReplaySubject.asObservable();

  constructor() {
    if (localStorage.getItem('tasksIdInc')) {
      this.tasksIdInc = JSON.parse(localStorage.getItem('tasksIdInc'));
    }
    if (localStorage.getItem('tasks')) {
      this.tasksReplaySubject.next(JSON.parse(localStorage.getItem('tasks')));
    }
    this._$['tasks'] = this.tasksReplaySubject.subscribe(tasks => {
      localStorage.setItem('tasks', JSON.stringify(tasks));
      this.tasks = tasks;
    });
  }

  addTask(task: ITask) {
    this.tasksIdInc++;
    localStorage.setItem('tasksIdInc', JSON.stringify(this.tasksIdInc));
    this.tasks.unshift(Object.assign({id: this.tasksIdInc}, task));
    this.tasksReplaySubject.next(this.tasks);
  }

  deleteTask(id: number) {
    this.tasks.splice(this.tasks.findIndex(task => task.id === id), 1);
    this.tasksReplaySubject.next(this.tasks);
  }

  getTask(id: number) {
    return this.tasks.find(t => t.id === id);
  }

  updateTask(id: number, data: any) {
    const task = this.tasks.find(t => t.id === id);
    Object.assign(task, data);
    this.tasksReplaySubject.next(this.tasks);
  }

  move(from, to) {
    this.tasks.splice(to, 0, this.tasks.splice(from, 1)[0]);
    this.tasksReplaySubject.next(this.tasks);
  }

  ngOnDestroy(): void {
    Object.keys(this._$).map((key) => {
      this._$[key].unsubscribe();
    });
  }
}

export interface ITask {
  id?: number;
  status: 'todo' | 'processing' | 'done';
  name: string;
  description: string;
  expireAtMS: number;
}
