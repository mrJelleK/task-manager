import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppStorageService, ITask} from '../app-storage.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-task-editor',
  templateUrl: './task-editor.component.html',
  styleUrls: ['./task-editor.component.scss']
})
export class TaskEditorComponent implements OnInit, OnDestroy {
  task: ITask = {description: null, expireAtMS: null, name: null, status: 'todo'};
  _$: Array<Subscription> = [];

  taskId;
  form: FormGroup = new FormGroup({
      name: new FormControl(this.task.name, [Validators.required]),
      description: new FormControl(this.task.description, [Validators.required]),
      expireAtMS: new FormControl(this.task.expireAtMS, [Validators.required]),
      status: new FormControl(this.task.status, [Validators.required]),
    }
  );

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private appStorageService: AppStorageService,
  ) {
  }

  ngOnInit() {
    this._$['params'] = this.route.params
      .subscribe((params) => {
        this.taskId = +params['id'];
        this.getTask();
      });
  }

  getTask() {
    if (this.taskId) {
      this.task = this.appStorageService.getTask(this.taskId);
      if (this.task === undefined) {
        this.router.navigate(['']);
        return;
      }
      this.form.setValue({
        name: this.task.name,
        description: this.task.description,
        expireAtMS: this.task.expireAtMS === null ? null : new Date(this.task.expireAtMS),
        status: this.task.status
      });
    }
    this.form.setValue({
      name: this.task.name,
      description: this.task.description,
      expireAtMS: this.task.expireAtMS === null ? null : new Date(this.task.expireAtMS),
      status: this.task.status
    });
  }

  onSaveButtonClick() {
    this.save();
  }

  onCancelButtonClick() {
    this.router.navigate(['']);
  }

  save() {
    if (this.form.invalid) {
      return;
    }
    const data: ITask = this.form.value;
    data.expireAtMS = +new Date(this.form.value.expireAtMS);

    if ([null, undefined].indexOf(this.task.id) === -1) {
      this.appStorageService.updateTask(this.task.id, data);
      this.router.navigate(['']);
    } else {
      this.appStorageService.addTask(data);
      this.router.navigate(['']);
    }
  }

  ngOnDestroy(): void {
    Object.keys(this._$).map((key) => {
      this._$[key].unsubscribe();
    });
  }
}
