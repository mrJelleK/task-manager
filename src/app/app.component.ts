import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppStorageService, ITask} from './app-storage.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy, OnInit {
  title = 'task-manager';
  tasks: Array<ITask>;
  startIdx: number;

  _$: Array<Subscription> = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private appStorageService: AppStorageService
  ) {
  }

  ngOnInit(): void {
    this._$['tasks'] = this.appStorageService.tasks$.subscribe(tasks => {
      this.tasks = tasks;
    });
  }

  onAddTaskButtonClick() {
    this.addTask();
  }

  addTask() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

  editTask(task: ITask) {
    this.router.navigate([`${task.id}`], {relativeTo: this.route});
  }

  deleteTask(id: number) {
    if (this._$['delete']) {
      this._$['delete'].unsubscribe();
    }
    this.appStorageService.deleteTask(id);
  }

  onDragStart(event: DragEvent, task, idx) {
    event.dataTransfer.effectAllowed = 'move';
    event.dataTransfer.setData('idx', idx);
    this.startIdx = idx;
  }

  onDragEnd(event: DragEvent, task, idx) {
    return false;
  }

  onDrop(event: DragEvent, task, idx) {
    this.appStorageService.move(this.startIdx, idx);
    return false;
  }

  onDragOver(event: DragEvent, task, idx: number) {
    event.dataTransfer.dropEffect = 'move';
    return false;
  }


  ngOnDestroy(): void {
    Object.keys(this._$).map((key) => {
      this._$[key].unsubscribe();
    });
  }
}
