import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AppStorageService, ITask} from '../app-storage.service';

@Component({
  selector: 'app-task-card',
  templateUrl: './task-card.component.html',
  styleUrls: ['./task-card.component.scss']
})
export class TaskCardComponent implements OnInit {
  @Input() task: ITask = {description: 'description', expireAtMS: Date.now(), name: 'name', status: 'todo'};
  @Output() taskDeleteClicked = new EventEmitter();
  @Output() taskEditClicked = new EventEmitter();
  currentDateMS = Date.now();

  constructor(private appStorageService: AppStorageService) {
  }

  ngOnInit() {
  }

  onDeleteTaskButtonClick() {
    this.deleteTask();
  }

  editTask() {
    this.taskEditClicked.emit(this.task);
  }

  deleteTask() {
    this.taskDeleteClicked.emit(this.task.id);
  }

  onEditTaskButtonClick() {
    this.editTask();
  }

  setStatus(status: 'todo' | 'processing' | 'done') {
    const data = {status: status};
    this.appStorageService.updateTask(this.task.id, data);
  }
}
