import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TaskEditorComponent} from './task-editor/task-editor.component';

const routes: Routes = [
  {path: 'new', component: TaskEditorComponent},
  {path: ':id', component: TaskEditorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
